.. image:: https://jazzband.co/static/img/badge.svg
   :target: https://jazzband.co/
   :alt: Jazzband

.. image:: https://img.shields.io/pypi/v/wagtailmenus.svg
   :target: https://pypi.python.org/pypi/wagtailmenus
   :alt: PyPI Download

.. image:: https://img.shields.io/pypi/v/wagtailmenus.svg
    :alt: PyPi Version
    :target: https://pypi.python.org/pypi/wagtailmenus

.. image:: https://github.com/jazzband/wagtailmenus/actions/workflows/test.yml/badge.svg
    :alt: Build Status
    :target: https://github.com/jazzband/wagtailmenus/actions/workflows/test.yml

.. image:: https://codecov.io/gh/jazzband/wagtailmenus/branch/master/graph/badge.svg
    :alt: Code coverage
    :target: https://codecov.io/gh/jazzband/wagtailmenus

.. image:: https://readthedocs.org/projects/wagtailmenus/badge/?version=latest
    :alt: Documentation Status
    :target: http://wagtailmenus.readthedocs.io/en/latest/


NOTE: this is a 'friendly fork' of the `wagtail-menus <https://github.com/rkhleics/wagtailmenus>`_ project intended to be customized for the `WagPress Project <https://gitlab.com/lansharkconsulting/wagpress>`_.

==============
wagpress-menus
==============

wagpress-menus is an extension for Torchbox's `Wagtail CMS <https://github.com/torchbox/wagtail>`_ to help you manage and render multi-level navigation and simple flat menus in a consistent, flexible way.

The current version is tested for compatibility with the following:

- Wagtail versions >2.15, 3.0, and 4.0
- Django versions 3.0, 4.0, and 4.1
- Python versions 3.7 to 3.10

.. image:: https://raw.githubusercontent.com/jazzband/wagtailmenus/master/docs/source/_static/images/repeating-item.png

New to wagtailmenus?
====================

Start by reading our `Overview and key concepts <http://wagtailmenus.readthedocs.io/en/stable/overview.html>`_ page.
It will give you a better understand of how the app works, and help you decide whether it is a good fit for your project.


How do I use it?
================

For everything you need to get up and running with wagpress-menus, `view the official documentation <http://wagtailmenus.readthedocs.io/en/latest/>`_.


Contributing
============

Want to contribute to wagpress-menus? We'd be happy to have you! You should start by taking a look at our `Contributor guidelines <http://wagtailmenus.readthedocs.io/en/latest/contributing/index.html>`_

As we are members of a `JazzBand project <https://jazzband.co/projects>`_, `wagtailmenus` contributors should adhere to the `Contributor Code of Conduct <https://jazzband.co/about/conduct>`_.
